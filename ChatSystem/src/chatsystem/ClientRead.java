/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem;

/**
 *
 * @author trupt
 */
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class ClientRead extends Thread{
	Socket socket;
        HashMap<String,JPanel> msgPanel;
        HashMap<JPanel,String> userList;
        JPanel jPanel1,jPanelUser;
        String user;
        
	public ClientRead(Socket socket,ClientWindow obj){
		this.socket = socket;
                this.msgPanel=obj.msgPanel;
                this.userList=obj.userList;
                this.jPanel1=obj.jPanel1;
                this.jPanelUser=obj.jPanelUser;
                this.user=obj.user;
                
	}

	public void run(){
		try{
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(),true);
                        output.println(user);
			while(true){
				String echoString = input.readLine();
				if(echoString!=null){
                                String[] name_message=echoString.split(":");
				String name=name_message[0];
                                System.out.println(name);
				String message=name_message[1];
                                System.out.println(message);
                                if(name.equals("user")){
                                    Random rand = new Random();
                                    float r = rand.nextFloat();
                                    float g = rand.nextFloat();
                                    float b = rand.nextFloat();
                                    Color c =new Color(r,g,b);
                                    JPanel p=new JPanel();
                                    JPanel p1=new JPanel();
                                    p1.setOpaque(true);
                                    p1.setBackground(c);
                                    p1.setSize(jPanel1.getSize());
                                    System.out.println(jPanel1.getSize().toString());
                                    JLabel l=new JLabel(message);
//                                    p1.add(l);
                                    Font  f  = new Font(Font.SANS_SERIF,  Font.PLAIN, 30);
                                    Border bd = BorderFactory.createLineBorder(Color.blue);
                                    l.setFont(f);
                                    p.setBorder(bd);
                                    p.setOpaque(true);
                                    p.add(l);
                                    userList.put(p,message);
                        //            System.out.println(p1);
                                    msgPanel.put(message, p1);
                                    jPanelUser.add(p);
                                }
                                else if(name.equals("online")){
                                    for(Map.Entry<JPanel,String> list:userList.entrySet()){
                                        if(list.getValue().equals(message)){
                                            JPanel p=list.getKey();
                                            p.setBackground(Color.GREEN);
                                            break;
                                        }
                                    }
                                }else if(name.equals("offline")){
                                    for(Map.Entry<JPanel,String> list:userList.entrySet()){
                                        if(list.getValue().equals(message)){
                                            JPanel p=list.getKey();
                                            p.setBackground(null);
                                            break;
                                        }
                                    }
                                }else{
				JPanel p=msgPanel.get(name);
                                JLabel lbMessage=new JLabel(message);
                                JPanel m=new JPanel();
                                m.add(lbMessage);
                                p.add(m);
                               
                                p.validate();
                                }
                                }
                                
				// try{
				// 	Thread.sleep(1000);
				// }
				// catch(InterruptedException e){
				// 	System.out.println("InterruptedException Caught!");
				// }
				// System.out.println("Message Sent :"+echoString);
				// output.println("Server Got this : "+echoString);
			}
		}
		catch(IOException e){
			System.out.println("Exception in ClientRead : "+e);
		}
		finally{
			try{
				socket.close();
			}
			catch(IOException e){

			}
		}
	}
}
