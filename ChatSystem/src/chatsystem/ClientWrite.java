/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem;

/**
 *
 * @author trupt
 */
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class ClientWrite extends Thread{
	Socket socket;
        HashMap<String,JPanel> messagePanel;
        HashMap<JPanel,String> userList;
        JButton btnSend;
        JTextField txtMessage;
        JPanel selectedUserPanel;
        JPanel selectedMessagePanel;
        String selectedUser;
        String user;
        JPanel jPanel1;
        ClientWindow obj;
	public ClientWrite(Socket socket,ClientWindow obj,JButton btnSend,JTextField txtMessage){
		this.socket=socket;
                this.btnSend=btnSend;
                this.txtMessage=txtMessage;
                this.user=obj.user;
                this.messagePanel=obj.msgPanel;
                this.userList=obj.userList;
                this.jPanel1=obj.jPanel1;
                this.obj=obj;
	}
//        private void setNoBackground(){
//        for (Map.Entry mapElement : userList.entrySet()) { 
//            JPanel p=(JPanel)mapElement.getKey();
//            p.setBackground(null);
//        } 
//    }

        @Override
	public void run(){
		try{
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
//                        output.println(user);
                        btnSend.addActionListener(new ActionListener(){
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                
//                                System.out.println(txtMessage.getText());
                                String message=txtMessage.getText();
                                output.println(selectedUser+":"+message);
                                JLabel lbMessage=new JLabel(message);
                                JPanel messagePanelNew=new JPanel();
                                messagePanelNew.add(lbMessage);
                                System.out.println(selectedUser+":"+message);
                                selectedMessagePanel.add(messagePanelNew);
                                txtMessage.setText("");
                                selectedMessagePanel.validate();
                                
                            }
                        });
                        obj.addWindowListener(new WindowListener(){
                            @Override
                            public void windowOpened(WindowEvent e) {
//                                ?throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public void windowClosing(WindowEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                                    output.println("remove:"+user);
                                  try{
                                      socket.close();
                                  }catch(IOException o){
                                      System.out.println(o.getMessage());
                                  }
                            }

                            @Override
                            public void windowClosed(WindowEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                                  
                            }

                            @Override
                            public void windowIconified(WindowEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public void windowDeiconified(WindowEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public void windowActivated(WindowEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public void windowDeactivated(WindowEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }
                        });
                        for(Map.Entry<JPanel,String> i:userList.entrySet()){
                            JPanel p=i.getKey();
                            String u=i.getValue();
                            p.addMouseListener(new MouseListener(){
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                if(selectedUserPanel==null){
                                    selectedUserPanel=p;
                                }else{
                                    selectedUserPanel.setBorder(BorderFactory.createLineBorder(Color.blue));
                                    selectedUserPanel=p;
                                }
                                selectedUser=userList.get(p);
//                                setNoBackground();
                                selectedMessagePanel=messagePanel.get(u);
//                                System.out.print(selectedMessagePanel);
                                jPanel1.removeAll();
                                jPanel1.add(selectedMessagePanel);
                                p.setBorder(BorderFactory.createMatteBorder(1, 8, 1, 1, Color.BLUE));
                                obj.validate();
                                obj.repaint();
                                btnSend.setEnabled(true);
                                txtMessage.setEnabled(true);
                                
                            }

                            @Override
                            public void mousePressed(MouseEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public void mouseReleased(MouseEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public void mouseEntered(MouseEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public void mouseExited(MouseEvent e) {
//                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }
                        });
                        }

			// System.out.println("connected to echoerwrite");
		}
		catch(IOException e){

			System.out.println("Exception in ClientWrite : "+e);
		}
		finally{
//			try{
//				socket.close();
//			}
//			catch(IOException e){
//
//			System.out.println("Exception in closing ClientWrite socket");
//		}
		}
	}
}