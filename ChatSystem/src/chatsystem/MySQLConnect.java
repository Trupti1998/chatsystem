/*

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
package chatsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class MySQLConnect {
    /**
     * @return Connection
     */
    public static Connection getConnection(){
        Connection conn=null;
        try{
            conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/chat_system", "studylink", "studylink");
//            JOptionPane.showMessageDialog(null,"Connection Established!");
            
        }
        catch(SQLException ex){
        JOptionPane.showMessageDialog(null,"Connection Failed: "+ex.getMessage());
        }
        return conn;
    }
//    public static void main(String args[]){
//        Connection conn= getConnection();
//    }
    
}
