/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem;

/**
 *
 * @author trupt
 */
import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.JOptionPane;
public class Server{
	static HashMap<String,Socket> clients =new HashMap<String,Socket>();
        private static Connection conn;
        private static PreparedStatement ps;
        private static ResultSet rs=null;
	public static void main(String[] args) {
		try(ServerSocket serverSocket = new ServerSocket(5000)){
			while(true){
				Socket socket = serverSocket.accept();
                                System.out.println("accepted");
				BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String name=input.readLine();
                                setUserOnline(name);
                                System.out.println("got name:"+name);
				setGreen(clients,name);
                                clients.put(name,socket);
				new ServerRead(name,socket,clients).start();
				// new ServerWrite(socket,clients).start();
			}
		}
		catch(IOException e){
			System.out.println("Server Exception : "+e);
		}
	}
        private static void setGreen(HashMap<String,Socket> clients,String name) throws IOException{
                for(String key:clients.keySet()){
                    Socket receiver = clients.get(key);
                    PrintWriter receiverOutput = new PrintWriter(receiver.getOutputStream(),true);
                    String message="online:"+name;
                    receiverOutput.println(message);
                }
        }
        
        private static void setUserOnline(String name){
            try{
               conn=MySQLConnect.getConnection();
                String sql="update user set is_online=? where uname=?";
                ps=conn.prepareStatement(sql);
                ps.setInt(1,1);
                ps.setString(2,name);
                int n=ps.executeUpdate();
           }
           catch(SQLException e){
            JOptionPane.showMessageDialog(null,e.getMessage());   
           }
        }
}



