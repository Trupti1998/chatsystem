/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem;

/**
 *
 * @author trupt
 */
import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.JOptionPane;

public class ServerRead extends Thread{
	Socket socket;
	HashMap<String,Socket> clients;
	String sender;
        private static Connection conn;
        private static PreparedStatement ps;
        private static ResultSet rs=null;
	public ServerRead(String sender,Socket socket,HashMap<String,Socket> clients){
		this.socket = socket;
		this.clients=clients;
		this.sender=sender;
                conn=MySQLConnect.getConnection();

	}
        private static void setOffline(String name){
            try{
               conn=MySQLConnect.getConnection();
                String sql="update user set is_online=? where uname=?";
                ps=conn.prepareStatement(sql);
                ps.setInt(1,0);
                ps.setString(2,name);
                int n=ps.executeUpdate();
           }
           catch(SQLException e){
            JOptionPane.showMessageDialog(null,e.getMessage());   
           }
        }
        private void setNoGreen(HashMap<String,Socket> clients,String name) throws IOException{
                for(String key:clients.keySet()){
                    Socket receiver = clients.get(key);
                    PrintWriter receiverOutput = new PrintWriter(receiver.getOutputStream(),true);
                    String message="offline:"+name;
                    receiverOutput.println(message);
                }
        }
        private void setGreen(HashMap<String,Socket> clients,PrintWriter obj) throws IOException{
                for(String key:clients.keySet()){
                    obj.println("online:"+key);
                }
        }
        private void setUserListUI(PrintWriter obj,String sender){
            try{
                String sql="select * from user where uname<>? ";
                ps=conn.prepareStatement(sql);
                ps.setString(1,sender);
                rs=ps.executeQuery();
                while(rs.next()){
                    obj.println("user:"+rs.getString("uname"));
                }
        
            }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"error in userlist retrieval");
                }
        }

	public void run(){
		try{
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(),true);
                        setUserListUI(output,sender);
                        setGreen(clients,output);
			while(true){
				String echoString = input.readLine();
				if(echoString!=null){
                                String[] name_message=echoString.split(":");
				String name=name_message[0];
				String message=name_message[1];
                                System.out.println(name);
                                System.out.println(message);
                                if(name.equals("remove")){
                                    setOffline(message);
                                    clients.remove(message);
                                    setNoGreen(clients,message);
                                }
                                else if(clients.containsKey(name)){
					Socket receiver = clients.get(name);
					PrintWriter receiverOutput = new PrintWriter(receiver.getOutputStream(),true);
					message=sender+":"+message;
					receiverOutput.println(message);
				}else{
					output.println(name +":not available to chat!!");
				}
                                }

				// try{
				// 	Thread.sleep(1000);
				// }
				// catch(InterruptedException e){
				// 	System.out.println("InterruptedException Caught!");
				// }
				// System.out.println("Message Sent :"+echoString);
				// output.println("Server Got this : "+echoString);
			}
		}
		catch(IOException e){
			System.out.println("Exception in ServerRead : "+e);
		}
		finally{
			try{
				socket.close();
			}
			catch(IOException e){
				System.out.println("Exception in ServerRead socket: "+e);
			}
		}
	}
}
